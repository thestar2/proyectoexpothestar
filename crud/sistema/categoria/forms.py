from django import forms
from .models import *
from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import categorias, Cuenta, Colecciones, Productos

class categoriaForm(forms.ModelForm):
    class Meta:
        model = categorias
        fields = '__all__'

class cuentaForm(forms.ModelForm):
    class Meta:
        model = Cuenta
        fields = '__all__'

class coleccionesForm(forms.ModelForm):
    class Meta:
        model = Colecciones
        fields = '__all__'

class ProductosForm(forms.ModelForm):
    class Meta:
        model = Productos
        fields = '__all__' 



class SignupForm(UserCreationForm):
    username = forms.CharField(max_length=16, widget=forms.TextInput(attrs={"type":"text", "class":"bx bx-user"}))
    first_name = forms.CharField(max_length=16, widget=forms.TextInput(attrs={"type":"text", "class":"bx bx-user"}))
    last_name = forms.CharField(max_length=16, widget=forms.TextInput(attrs={"type":"text", "class":"bx bx-user"}))
    email = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"bx bx-user"}))
